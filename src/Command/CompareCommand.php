<?php

namespace App\Command;

use Apache\Solr\HttpTransport\ApacheSolrHttpTransportCurl;
use App\Services\Compare\CompareServiceInterface;
use Carid\SolrQueryBuilder\Carid\Exception;
use Carid\SolrQueryBuilder\Carid\Solr\Condition;
use Carid\SolrQueryBuilder\Carid\Solr\ConditionBlock;
use Carid\SolrQueryBuilder\Carid\Solr\IndexAdapter;
use Carid\SolrQueryBuilder\Carid\SolrConfig;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompareCommand extends Command
{
    protected static $defaultName = 'app:compare';

    /** @var CompareServiceInterface */
    private $compareService;

    public function __construct(CompareServiceInterface $compareService, string $name = null)
    {
        $this->compareService = $compareService;

        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('type', InputArgument::REQUIRED, 'Type of product');
        $this->setDescription('Compare solr products')
            ->setHelp('This command compare solr products');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            'COMPARE STARTED!',
            ''
        ]);

        $type = $input->getArgument('type');

        $this->compareService->compare($type);

        return Command::SUCCESS;
    }
}
