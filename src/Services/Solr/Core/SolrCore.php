<?php

namespace App\Services\Solr\Core;

use Apache\Solr\HttpTransport\ApacheSolrHttpTransportCurl;
use Carid\SolrQueryBuilder\Carid\Exception;
use Carid\SolrQueryBuilder\Carid\Solr\IndexAdapter;
use Carid\SolrQueryBuilder\Carid\SolrConfig;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class SolrCore
{
    /** @var ContainerBagInterface  */
    private $params;

    /**
     * SolrCore constructor.
     * @param ContainerBagInterface $params
     */
    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @var SolrConfig
     */
    private $solrConfig;

    private function init($coreName): void
    {
        $solrConfigClass = new SolrConfig();

        $solrConfigClass->setHttpTransport(new ApacheSolrHttpTransportCurl());
        $solrConfigClass->setDocId($this->params->get('solr.cores.' . $coreName . '.docId'));
        $solrConfigClass->setCoreName($this->params->get('solr.cores.' . $coreName . '.name'));
        $solrConfigClass->setPath($this->params->get('solr.path'));
        $solrConfigClass->setHost($this->params->get('solr.' . $coreName . '.host'));
        $solrConfigClass->setPort($this->params->get('solr.' . $coreName . '.port'));
        $solrConfigClass->setSsl($this->params->get('solr.ssl'));

        $this->solrConfig = $solrConfigClass;
    }

    /**
     * @param $name string Name of index
     * @return IndexAdapter
     * @throws Exception
     */
    public function getIndexByName($name): IndexAdapter
    {
        $indexAdapter = new IndexAdapter($this->getSolrConfigByName($name));

        $indexAdapter->setCoreName($name);

        return $indexAdapter;
    }


    private function getSolrConfigByName($coreName): SolrConfig
    {
        $this->init($coreName);

        return $this->solrConfig;
    }
}
