<?php

namespace App\Services\Solr;

class Values
{
    //superprod - 60, configurator - 50, parent - 40, mpn - 30 , single - 20 , child - 10
    const TYPE_ORDER_SUPER_PRODUCTS  = '60';
    const TYPE_ORDER_CONFIGURATOR    = '50';
    const TYPE_ORDER_PARENT_PRODUCTS = '40';
    const TYPE_ORDER_MPN_PRODUCTS    = '30';
    const TYPE_ORDER_SINGLE_PRODUCTS = '20';
    const TYPE_ORDER_CHILD_PRODUCTS  = '10';
    const HAS_FITMENT                = 1;
    const HAS_FITMENT_UNKNOWN        = -1;
    const HAS_SINGLE_YES = 1;
    const HAS_IMAGE = 1;
    const HAS_SINGLE_NO = 0;
    /** @deprecated */
    const SINGLE_IN_TEMPLATE_YES = 1;
    const IN_SUPER = 1;

    const VISIBILITY_LIVE  = 'live';
    const NOT_IN_SUPER = 0;
    const FOR_SALE_Y = 'Y';
    const FOR_SALE_H = 'H';
    const FOR_SALE_N = 'N';
    const MMY_GROUP_IDS_EMPTY = 0;
    const MMY_GROUPS_EMPTY_INT = 0;
    const IS_CONFIGURATOR_YES = 1;
    const IS_CONFIGURATOR_NO = 0;
    const STOCK_STATUS_YES = 1;
    const STOCK_STATUS_NO = 0;

    const PRODUCT_TYPE_ORDER_SINGLE_PRODUCTS       = '10';
    const PRODUCT_TYPE_ORDER_CONFIGURATOR_PRODUCTS = '20';
    const PRODUCT_TYPE_ORDER_PARENT_PRODUCTS       = '30';
    //TODO: CHECK IT IN ALL USAGES
    const FITMENT_TYPE_VEHICLE_SPECIFIC = 1;

    const MPN_PRODUCT_PREFIX = 'mpn';
    const SUPER_PRODUCT_PREFIX = 'sp';
    const ADDED_LAST_DAYS_YES = 1;

    const MMY_DELIMITER = '|';
}
