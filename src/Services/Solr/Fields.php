<?php

namespace App\Services\Solr;

class Fields
{
    const ID            = 'id';
    /**
     * Used in single product to get theirs mpn id
     */
    const MPN_ID = 'mpn_id';
    const TYPE_ORDER    = 'type_order_catalog';
    const PRODUCT_ID    = 'product_id';
    const SYSTEM_ID    = 'system_id';
    const PLAIN_TITLE = 'title';
    const TITLE         = 'title_catalog';
    const TITLE_STRING  = 'title_catalog_string';
    const PRICE_MIN     = 'price_min';
    const PRICE_MAX     = 'price_max';
    const CALL_FOR_PRICE = 'call_for_price';
    const IMAGE         = 'img_url';
    const IMAGE_SMALL   = 'img_url_small';
    const MASK_IMAGE   = 'mask_image';
    const DESCRIPTION   = 'description';
    const DESCRIPTION_UNRESOLVED   = 'description_unresolved';
    const DESCRIPTION_WITHOUT_MMY_TAGS = 'description_without_mmy_tags';
    const URL           = 'product_url';
    const RATING_COUNT  = 'rating_count';
    const RATING_STARS  = 'rating_avg';
    const SKU           = 'sku';
    const PRICE_RANGE   = 'price_ranges';
    const MODELS        = 'models';
    const YEARS         = 'years';
    const HAS_FITMENT   = 'has_fitment';
    const FITMENT_MAKE_MODEL_ALL = 2;
    const FITMENT_CUSTOM_BUILD = 7;
    const HAS_SINGLE    = 'has_single';
    const FITMENT_TYPE  = 'fitment_type';
    const SP_IDS        = 'sp_ids';
    const MAKES_FIELD   = 'makes';
    const CHILD_PRODUCT_IDS_FIELD = 'child_product_ids';

    const CREATED_AT        = 'created_at';
    const ADDED_LAST_DAYS   = 'added_last_30days';

    const MANUFACTURER_ID = 'manufacturer_id';
    const TYPE_GROUP_ID = 'type_group_id';
    const DEPARTMENT_ID = 'department_id';
    const TYPE_IDS = 'type_ids';
    const MAIN_PTYPE_ID = 'main_ptype_id';
    const MMY_GROUP_IDS = 'mmy_group_ids';
    const UNIT_TYPE_IDS = 'unit_type_ids';
    const FACETS_FIELD = 'facets';

    const CUSTOM_PRODUCT_PTYPE_ORDER = 'custom_product_ptype_order';
    const CUSTOM_PRODUCT_DEPARTMENT_ORDER = 'custom_product_department_order';
    const CUSTOM_PTYPE_ORDER = 'custom_ptype_order';
    const PTYPE_ROLE = 'ptype_role';
    const PTYPE_SALES_ORDER = 'ptype_sales_order';
    const PTYPE_MANUFACTURER_SALES_ORDER = 'ptype_manufacturer_sales_order';
    const DEPARTMENT_PTYPE_SALES_ORDER = 'department_ptype_sales_order_';
    const DEPARTMENT_PTYPE_MANUFACTURER_SALES_ORDER = 'department_ptype_manufacturer_sales_order_';
    const DEPARTMENT_TEMPLATE_SALES_ORDER = 'department_template_sales_order_';
    const TEMPLATE_SALES_ORDER = 'template_sales_order';
    const RELATED_MMY_TEMPLATE_SALES_QUANTITY = 'related_mmy_template_sales_quantity';
    const TEMPLATE_SALES_QUANTITY = 'template_sales_quantity';
    const MMY_QUANTITY = 'mmy_quantity';
    const DEPARTMENT_RECOMMENDED_ORDER = 'dro';

    /**
     * USE for signle which not in super
     */
    const IN_SUPER = 'in_super';
    const IN_SUPER_V2 = 'in_super_v2';
    const YMM           = 'ymm';
    const MMY           = 'mmy';
    const MMY_DELIMITER = '|';
    const VEHICLE_SUB_TYPE = 'vehicle_subtype';
    const VEHICLE_TYPE = 'vehicle_type';
    const STORE = 'store';
    const STOCK_STATUS  = 'stock_status';
    const ALTERNATE_MPN_FOR_SORT  = 'alternate_mpn_for_sort_';
    const ALTERNATE_OE_FOR_SORT  = 'alternate_oe_for_sort_';
    const SORT_FIELD_PREFIX = 'recommended_order_ptype_group_';
    const IS_CONFIGURATOR = "is_configurator";
    const IS_RECOMMENDED = "is_recommended";
    const IS_OE_BRAND = 'is_oe_brand';
    const FORSALE   = 'forsale';
    const QUANTITY   = 'quantity';
    const COLORS = 'colors';
    const FEATURES = 'features';
    const FEATURE_IMAGE = 'feature_image';
    const EXTENDED_FEATURES = 'extended_features';
    const MAIN_PTYPE_GROUP_ID_FIELD = 'mainPtypeGroupId';
    const MMY_COUNT   = 'mmy_count';
    const PRODUCT_TYPE_ORDER = 'type_order';
    const CHILD_PRODUCT_URL = 'product_url_single_mpn';
    const TYPES_COUNT = 'types_count';
    const BRAND_ORDER = 'brand_order';
    const FEATURED_360_VIEW = 'featured_360_view';
    const REFINE_FITMENT_FIELD_MMY = 'refine_fitment_field_mmy_*';
    const ALTERNATE_MPN_FOR = 'alternate_mpn_for';
    const ALTERNATE_OE_FOR = 'alternate_oe_for';
    const VISIBILITY          = 'visibility';
    const HAS_IMAGE = 'has_image';
    const CANONICAL_VEHICLE_TYPE = 'canonical_vehicle';
    const CANONICAL_STORE = 'canonical_store';
    const MANUFACTURER_PTYPE_GROUP_WEIGHT = 'manufacturer_ptype_group_weight';
    const CUSTOM_DEPARTMENT_ORDER_PREFIX_STRING_V2 = 'custom_order_department_v2_';
    const CUSTOM_ORDER_PREFIX_STRING_V3 = 'custom_order_ptype_group_v3_';
    const CUSTOM_ORDER_PREFIX_STRING_V4 = 'custom_order_ptype_group_v4_';
    const CUSTOM_PTYPE_ORDER_PREFIX_STRING_V2 = 'custom_order_ptypes_v2_';
    const CUSTOM_PTYPE_ORDER_VISIBILITY_V2 = 'custom_order_ptypes_visibility_v2_';
    const CUSTOM_DEPARTMENTS_ORDER_VISIBILITY_V2 = 'custom_order_departments_visibility_v2_';
    const CUSTOM_PTYPE_GROUPS_ORDER_VISIBILITY_V2 = 'custom_order_ptype_groups_visibility_v2_';
    const CUSTOM_ORDER_VISIBILITY_YES_INT = 1;
    const CUSTOM_ORDER_VISIBILITY_NO_INT = 0;
    const DEFAULT_VEHICLE_TYPE_SUBTYPE_STORE_VALUES = '0_0_0';

    const CUSTOM_FIELDS                    = 'custom_fields';
    const FACET_CUSTOM_FIELDS_PTYPE_PREFIX = 'custom_fields_refine_search_ptype_';
    const FACET_CUSTOM_FIELD_PREFIX        = 'custom_field_refine_search_';
    /**
     * Use it for mmy refine sub models
     */
    const REFINE_MMY_FIELDS_PREFIX = 'refine_fitment_fields_mmy_';
    /**
     * Use it for mmy refine sub models
     */
    const REFINE_MMY_FIELD_PREFIX = 'refine_fitment_field_mmy_';

    const YEAR_VEHICLE = 'year_vehicle';
    const YEAR_MAKE_VEHICLE = 'year_make_vehicle';
    const YEAR_VEHICLE_DELIMITER = '|';
    const YEAR_MAKE_VEHICLE_DELIMITER = '|';

    public static $fieldList = array(
        self::TYPE_IDS,
        //        /**
        //         * TODO: add only on technical ptypes
        //         */
        self::TYPE_GROUP_ID,
        self::TYPE_ORDER,
        self::PRODUCT_ID,
        self::TITLE,
        self::PRICE_MIN,
        self::PRICE_MAX,
        self::CALL_FOR_PRICE,
        self::IMAGE_SMALL,
        self::FEATURE_IMAGE,
        self::DESCRIPTION,
        self::DESCRIPTION_UNRESOLVED,
        self::URL,
        self::RATING_COUNT,
        self::RATING_STARS,
        self::SKU,
        self::MANUFACTURER_ID,
        self::HAS_FITMENT,
        self::IS_CONFIGURATOR,
        self::IS_RECOMMENDED,
        self::FORSALE,
        self::COLORS,
        self::FEATURES,
        self::EXTENDED_FEATURES,
        self::CHILD_PRODUCT_URL,
        self::DEPARTMENT_ID,
        self::MMY_GROUP_IDS,
        self::MPN_ID,
        self::REFINE_FITMENT_FIELD_MMY,
        self::FITMENT_TYPE,
        self::FEATURED_360_VIEW,
        self::MASK_IMAGE,
        self::VEHICLE_TYPE,
        self::STORE,
        self::ADDED_LAST_DAYS,
        self::UNIT_TYPE_IDS
    );

    public static $baseFields = array(
        self::PRODUCT_ID
    );
}
