<?php

namespace App\Services\Compare;

interface CompareServiceInterface
{
    public function compare(string $type): void;
}
