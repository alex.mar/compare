<?php

namespace App\Services\Compare;

use App\Services\Logger\Logger;
use App\Services\Solr\Core\SolrCore;
use App\Services\Solr\Fields;
use App\Services\Solr\Values;
use Carid\SolrQueryBuilder\Carid\Exception;
use Carid\SolrQueryBuilder\Carid\Solr\Condition;
use Carid\SolrQueryBuilder\Carid\Solr\ConditionBlock;
use Carid\SolrQueryBuilder\Carid\Solr\IndexAdapter;

class CompareService implements CompareServiceInterface
{
    private const QUERY_LIMIT = 10;
    private const MANUFACTURERS_FACET_LIMIT = 100;

    private const STATUS_SUCCESS = 'success';
    private const STATUS_ERROR = 'error';

    private const NEW_FIELDS = [
        'type_group_ids' => true,
        'department_ids' => true,
        'has_single_context' => true
    ];
    private const NO_NEED_COMPARE = [
        '_version_' => true,
        'database' => true,
        'manufacturer_ptype_group_weight' => true,
//    'description' => true,
//    'description_unresolved' => true,
//    'description_without_mmy_tags' => true,
//    'img_url' => true,
//    'img_url_small' => true,
//    'canonical_vehicle' => true,
//    'canonical_store' => true
        'rating_count' => true,
        'rating_avg' => true
    ];

    /** @var Logger */
    private $logger;

    /** @var SolrCore */
    private $solrCore;

    public function __construct(
        Logger $logger,
        SolrCore $solrCore
    ) {
        $this->logger = $logger;
        $this->solrCore = $solrCore;
    }

    /**
     * @param string $type
     * @throws Exception
     */
    public function compare(string $type): void
    {
        $manufacturerIds = $this->getManufacturerIds();

        $result = [];
        foreach ($manufacturerIds as $manufacturerId) {
            $status = self::STATUS_SUCCESS;
            $core = $this->solrCore->getIndexByName('products');
            $core->setOption(IndexAdapter::PARAM_MINIMUM_MATCH, 1);
            $core->setFieldList(['*']);
            $core->setStats(false);
            $query = $core->getQuery();
            $query->disableFacet();
            $query->setLimit(self::QUERY_LIMIT);
            $query->setOffset(0);

            $query->addCondition(
                new Condition(
                    Fields::MANUFACTURER_ID,
                    $manufacturerId,
                    Condition::DEFAULT_PRIORITY,
                    false,
                    false,
                    false
                )
            );

            $conditionBlock = $this->getTypeProductCondition($type);

            $query->addConditionBlock($conditionBlock);
            $response = $query->executeQuery();
            $originalDocs = $response->getDocs();

            if (empty($originalDocs)) {
                $result[] = [
                    'ProductId' => '',
                    'Error' => 'Empty original brand ' . $manufacturerId,
                    'Field' => '',
                    'Value original' => '',
                    'Value branch' => ''
                ];
                $status = self::STATUS_ERROR;
            }

            if ($status !== self::STATUS_ERROR) {
                $productIds = [];
                foreach ($originalDocs as $doc) {
                    $productIds[] = $doc[Fields::PRODUCT_ID][0];
                }

                $core = $this->solrCore->getIndexByName('compare');
                $core->setOption(IndexAdapter::PARAM_MINIMUM_MATCH, 1);
                $core->setFieldList(['*']);
                $core->setStats(false);
                $query = $core->getQuery();
                $query->disableFacet();
                $query->setLimit(self::QUERY_LIMIT);

                $productsConditionBlock = $this->getByProductsCondition($productIds);

                $query->addConditionBlock($productsConditionBlock);
                $response = $query->executeQuery();
                $branchDocs = $response->getDocs();
                $indexedBranchDocs = [];
                foreach ($branchDocs as $branchDoc) {
                    $productId = $branchDoc[Fields::PRODUCT_ID][0];
                    $indexedBranchDocs[$productId] = $branchDoc;
                }

                foreach ($originalDocs as $originalDoc) {
                    $productId = $originalDoc[Fields::PRODUCT_ID][0];
                    if (!isset($indexedBranchDocs[$productId])) {
                        $result[] = [
                            'ProductId' => $productId,
                            'Error' => 'Product Not Found',
                            'Field' => '',
                            'Value original' => '',
                            'Value branch' => ''
                        ];
                        $status = self::STATUS_ERROR;
                        continue;
                    }
                    $branchDoc = $indexedBranchDocs[$productId];
                    foreach ($originalDoc as $field => $value) {
                        if (isset(self::NO_NEED_COMPARE[$field])) {
                            continue;
                        }
                        if (!isset($branchDoc[$field])) {
                            $result[] = [
                                'ProductId' => $productId,
                                'Error' => 'Field not exist',
                                'Field' => $field,
                                'Value original' => $this->formatValue($value),
                                'Value branch' => ''
                            ];
                            $status = self::STATUS_ERROR;
                            continue;
                        }
                        $branchValue = $branchDoc[$field];
                        if ($branchValue !== $value) {
                            $result[] = [
                                'ProductId' => $productId,
                                'Error' => 'Value not equal',
                                'Field' => $field,
                                'Value original' => $this->formatValue($value),
                                'Value branch' => $this->formatValue($branchValue)
                            ];
                            $status = self::STATUS_ERROR;
                        }
                    }
                    foreach ($branchDoc as $field => $value) {
                        if (isset(self::NEW_FIELDS[$field])) {
                            continue;
                        }
                        if (isset(self::NO_NEED_COMPARE[$field])) {
                            continue;
                        }
                        if (!isset($originalDoc[$field])) {
                            $result[] = [
                                'ProductId' => $productId,
                                'Error' => 'Extra field',
                                'Field' => $field,
                                'Value original' => '',
                                'Value branch' => $this->formatValue($value)
                            ];
                            $status = self::STATUS_ERROR;
                        }
                    }
                }
            }

            $this->logger->writeln('Brand ID ' . $manufacturerId . ' compared | ' . $status . ' |');
        }

        $fp = fopen('var/log/products.csv', 'wb');
        fputcsv($fp, ['ProductId', 'Error', 'Field', 'Value original', 'Value branch']);

        foreach ($result as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);

        $this->logger->writeln(PHP_EOL . 'COMPARE FINISHED!');
    }

    /**
     * @param mixed $value
     * @return string
     */
    private function formatValue($value): string
    {
        if (is_array($value)) {
            $value = implode(', ', $value);
        }

        return $value;
    }

    /**
     * @param string $type
     * @return ConditionBlock
     */
    private function getTypeProductCondition(string $type): ConditionBlock
    {
        $conditionBlock = new ConditionBlock();

        switch ($type) {
            case 'mpn':
                $value = Values::TYPE_ORDER_MPN_PRODUCTS;
                break;
            case 'single':
                $value = Values::TYPE_ORDER_SINGLE_PRODUCTS;
                break;
            case 'parent':
                $value = Values::TYPE_ORDER_PARENT_PRODUCTS;
                break;
            case 'super':
                $value = Values::TYPE_ORDER_SUPER_PRODUCTS;
                break;
            default:
                $value = null;
                break;
        }

        if ($value === null) {
            throw new \RuntimeException('Type product not defined');
        }

        $conditionBlock->addConditions([
            new Condition(
                Fields::TYPE_ORDER,
                $value,
                Condition::DEFAULT_PRIORITY,
                false,
                false,
                false
            )
        ]);

        return $conditionBlock;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getManufacturerIds(): array
    {
        $core = $this->solrCore->getIndexByName('products');
        $core->setOption(IndexAdapter::PARAM_MINIMUM_MATCH, 1);
        $core->setFieldList(['product_id']);
        $core->setStats(false);
        $core->setFacetLimit(self::MANUFACTURERS_FACET_LIMIT);
        $query = $core->getQuery();
        $query->enableFacet();
        $query->setFacetFields(['manufacturer_id']);
        $query->setLimit(0);
        $query->setOffset(0);
        $response = $query->executeQuery();

        $facets = $response->getFacets();

        return array_keys($facets['manufacturer_id']);
    }

    /**
     * @param array $productIds
     * @return ConditionBlock
     */
    public function getByProductsCondition(array $productIds): ConditionBlock
    {
        $productsConditionBlock = new ConditionBlock(ConditionBlock::OPERATOR_OR);
        foreach ($productIds as $productId) {
            $productsConditionBlock->addCondition(
                new Condition(
                    Fields::PRODUCT_ID,
                    $productId,
                    Condition::DEFAULT_PRIORITY,
                    false,
                    false,
                    false
                )
            );
        }

        return $productsConditionBlock;
    }
}
